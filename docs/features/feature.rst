########
Features
########

Secure Boot
***********

The firmware component of TRS unconditionally enables UEFI secure boot for all
supported platforms.  There are some hardware requirements that will dictate how
Secure Boot is configured and enabled on your hardware.

[UEFI]_ (§ 32.3.6 Platform Firmware Key Storage Requirements) defines that the
Platform and Key exchange keys must be stored in a non-volatile storage which
is tamper protected.

On Arm servers this is usually tackled by having a dedicated flash which is
only accessible by the secure world.

Hardware which was designed with security in mind has the following options.

======================== ================ ==============
Hardware                 UEFI Secure Boot Measured Boot
======================== ================ ==============
RPMB [1]_                        x              x
Discrete TPM                                    x
Flash in secure world            x
======================== ================ ==============

The reality on embedded boards is different though.  In the embedded case,
we don't have a dedicated flash.  What's becoming more common though is eMMC
devices with an RPMB partition.

If the board has a RPMB and OP-TEE support,  Trusted Substrate will use
that device to store all the EFI variables.

.. uml::

  skinparam sequenceMessageAlign center
  participant RPMB #ff5e13
  participant Supplicant #0093a6
  participant "U-Boot" as Uboot #0093a6
  participant "TF-A(OPTEED)" as TFA #CE5756
  participant "OP-TEE" as OPTEE #6b8724
  participant "OP-TEE PTA" as PTA #6b8724
  participant StMM as Stmm #7773cf

  Uboot --> TFA: TEEC_InvokeCommand
  TFA --> OPTEE: OP-TEE message
  PTA --> Stmm: MM buffer
  note over OPTEE,PTA #cccc00: PTA decapsulates the MM message\nfrom the OP-TEE message and\npasses it on to StMM
  Stmm --> OPTEE: FFA calls to OP-TEE Storage API for RPMB
  OPTEE --> Supplicant: RPC calls
  note over OPTEE,Uboot #cccc00: encrypt the data and send REE file operations to the supplicant through a series of RPC calls
  ...some time later...
  Supplicant --> RPMB: Read/Write to RPMB
  Supplicant --> Stmm: Read/Write finished
  Stmm --> PTA: MM buffer
  note over OPTEE,PTA #cccc00: PTA encapsulates the MM message,\ncreates an OP-TEE message and\npasses it on to OP-TEE
  OPTEE --> Uboot: OP-TEE response



However for boards that don't have an RPMB the UEFI public keys (PK, KEK, DB etc)
are built-in into the firmware binary. Bundling those keys comes with it's own
set of limitations.  The most notable ones being that in order to update any
security related EFI variable, you need to update the bootloader and you can
only boot signed binaries by default. Other, non security critical, EFI variables
are stored in a file located in the ESP.

.. [1] Requires OP-TEE support and a way to program the RPMB with a unique per hardware key
       (e.g a fuse accessible only from the secure world).
       Setting EFI variables at runtime (from the OS) not supported

Hardware and UEFI variable limitations
======================================

The firmware automatically enables and disables UEFI Secure Boot
based on the existence of the Platform Key (PK).  As a consequence
boards that embed the keys in the firmware binary will only be allowed to
boot signed binaries and you won't be able to change the UEFI keys.
See :ref:`Building with your own certificates`

On the other hand boards that store the variables in the RPMB come
with an empty PK and the user must provision one during the setup
process in order to enable Secure Boot.

.. uml::

  usecase "Board has OP-TEE?" as optee
  usecase "Board has RPMB?" as rpmb
  usecase "EFI variables in ESP.\nPK, KEK, db and dbx\nbuilt-in into the\nfirmware binary" as esp
  usecase "EFI variables in RPMB" as efirpmb
  usecase "Provision PK?" as provpk
  usecase "Secure Boot enabled" as sben
  usecase "Secure Boot disabled" as sbend

  rpmb -d-> optee: yes
  rpmb -d-> esp: no
  optee -d-> efirpmb: yes
  optee -r-> esp: no
  efirpmb -d-> provpk
  provpk -> sben: yes
  esp -> sben
  provpk -l-> sbend: no

Measured Boot
*************
TRS is designed to take advantage of Trusted Platform modules.  The firmware
part of TRS supports the `EFI TCG Protocol
<https://trustedcomputinggroup.org/resource/tcg-efi-protocol-specification/>`_
as well as `TCG PC Client Specific Platform Firmware Profile Specification
<https://trustedcomputinggroup.org/resource/pc-client-specific-platform-firmware-profile-specification/>`_
and provides the building blocks the OS needs for measured boot.

During the OS first boot it will automatically scan for a TPM.  If such a
device is present it will generate a random key, encrypt the root filesystem
and seal it against measurements found in `PCR7` which holds the Secure Boot
Policy and EFI keys used for UEFI Secure Boot.

Trusted Platform module
=======================

TPMs are microcontrollers designed for cryptographic tasks.
They contain a set of Platform Configuration Registers (PCRs) which are used to
measure the system configuration and software.

PCRs start zeroed out and can only reset with a system reboot.
Those can be extended by writing a SHA hash (typically SHA-1/256/384/512 for TPMv2)
into the PCR.  To store a new value in a PCR, the existing value is extended
with a new value as follows: PCR[N] = HASHalg( PCR[N] || ArgumentOfExtend )

Trusted Substrate is designed to work with either discrete TPMs or provide an
[fTPM]_ running in OP-TEE.

.. uml::

    usecase "Board has discrete TPM" as dtpm
    usecase "Board has RPMB & OP-TEE" as optee
    usecase "fTPM Trusted Applicaiton" as ftpm
    usecase "Measured Boot enabled" as mben
    usecase "Measured Boot disabled" as mbend

    dtpm -d-> mben: yes
    dtpm -r-> optee: no
    optee -d-> ftpm: yes
    ftpm -d-> mben: yes
    ftpm -d-> mbend: no

.. _op-tee-os:

LUKS2 disk encryption
*********************

If a TPM is present on the device TRS will automatically detect it,  generate a
random password on first boot,  seal it against PCR7 and encrypt your root
filesystem using aes-xts-plain.

TRS is designed to work regardless of the TPM implementation.  We support
devices with a discrete TPM, an [fTPM]_ or for QEMU a [SWTPM]_

.. uml::

    skinparam sequenceMessageAlign center
    participant "(f)TPM" as TPM #6b8724
    participant RPMB #ff5e13
    participant Supplicant #ff5e13
    participant "U-Boot" as Uboot #0093a6
    participant "Linux Kernel" as kernel #7773cf
    participant "initramfs" as initramfs #CE5756
    participant "rootfs" as rootfs #CE5756

    Uboot --> initramfs: Measure firmware OS-loader\n and OS to PCRs0-9
    |||
    note over RPMB, Supplicant #cccc00: If an fTPM is used OP-TEE \n and the tee-supplicant are\n required to store data to\n the RPMB
    |||
    initramfs --> TPM: On first boot \nSeal the encryption password\n against PCRs 7 & 9\n\n On subsequent boots\n Unseal the password
    |||
    initramfs -> rootfs: On first boot\n Create a LUKS volume \nEncrypt and mount roots\n\n On subsequent boots\n decrypt and mount rootfs
    |||
    initramfs --> reboot: reboot if decryption\n failed
    |||

.. note::
    You can find a full list of the components and recipes needed by running
    ``make find name=ledge-initramfs``

LUKS2 Encryption
================

.. image:: ../images/trs_encrypt.png
     :alt: LUKS2 filesystem encryption

LUKS2 Decryption
================

.. image:: ../images/trs_decrypt.png
     :alt: LUKS2 filesystem decryption

OP-TEE OS
*********
OP-TEE is our Secure World OS of choice in TRS.  We use it for a number of
reasons with the most notable ones being

- Run [fTPM]_ is the hardware doesn't have a discrete TPM.
- Store EFI variables on boards that have an RPMB.
- Provide a DRBG if the hardware doesn't provide a TRNG.
- Provide a PKCS#11 provider to PARSEC.

Conceptually the components interacting with OP-TEE in the TRS build can be seen
in the image below. The ``Features`` lane there indicates which exceptions
levels are involved in a certain use case. For example, "TEE: Secure Storage"
is all kept in (S)EL-0 and (S)EL-1.

.. image:: ../images/optee-trs.png
     :alt: OP-TEE TRS component overview

Note that this image is rather generic as depicted here. We have other areas
that could (and should) be added as well, for example ``SCMI``, ``Xen``,
``FF-A``, ``SwTPM`` to name a few. But perhaps it's better to add them as
separate diagrams to avoid making the images too complex.
